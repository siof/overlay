# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

EGIT_REPO_URI="git://github.com/lawl/opensnap.git
	https://github.com/lawl/opensnap.git"

inherit eutils git-2

DESCRIPTION="Opensnap brings the Aero Snap feature to Openbox."
HOMEPAGE="https://github.com/lawl/opensnap"
SRC_URI=""

LICENCE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="x11-misc/wmctrl
	x11-libs/gtk+:3
	x11-libs/libX11"

DEPEND="${RDEPEND}"

src_unpack() {
	git-2_src_unpack
	cd "${S}"
}

src_compile() {
	emake || die "emake failed"
}

src_install() {
#	emake DESTDIR="${D}" install || die "emake install failed"
	dobin bin/opensnap
	into /etc/skel
	dodir opensnap
	insinto /etc/skel/opensnap
	doins sample_configs/hit_bottom
	doins sample_configs/hit_left
	doins sample_configs/hit_right
	doins sample_configs/hit_top
}
