# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

EGIT_REPO_URI="git://github.com/bliet/opensnap_gui.git
	https://github.com/bliet/opensnap_gui.git"

inherit eutils git-2

DESCRIPTION="It is a little gui to run opensnap"
HOMEPAGE="https://github.com/bliet/opensnap_gui"
SRC_URI=""

LICENCE=""
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="dev-qt/qtcore
	x11-misc/opensnap"

DEPEND="${RDEPEND}"

src_unpack() {
	git-2_src_unpack
	cd "${S}"
}

src_compile() {
	qmake || die "qmake failed"
	emake || die "emake failed"
}

src_install() {
#	emake DESTDIR="${D}" install || die "emake install failed"
	ln -s ./OpenSnap opensnapgui
	dobin OpenSnap
	dobin opensnapgui
}
